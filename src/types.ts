export interface Authentication {
    authenticated: boolean,
    username: string | null,
    token: string | null,
}

export interface Preferences {
    features: Record<string, boolean>
}