import React, { FC } from "react";
import { RegisteredTimetableEntry, TimetableEntryType } from "../types";
import { computeEntryLayout } from "../utils";
import styled from "styled-components";

interface EntryProps {
    offset: number,
    width: number,
    background: string
}

const Entry = styled.div<EntryProps>`
    position: absolute;
    background: ${props => props.background};
    top: 5px;
    height: 40px;
    left: ${props => props.offset}%;
    width: ${props => props.width}%;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 10;
    border: 2px solid transparent;
`;

const TimetablePreviewRegisteredEntry: FC<RegisteredTimetableEntry> = (entry: RegisteredTimetableEntry) => {
    const { offset, width } = computeEntryLayout(entry.start, entry.end);
    const background = entry.type === TimetableEntryType.Lecture ? "#aaffaa" : "#aaddff";

    return (
        <Entry title={entry.name} offset={offset} width={width} background={background}>
            {
                entry.link !== null
                    ? <a href={entry.link} target="_blank" rel="noreferrer">{entry.code}</a>
                    : <span>{entry.code}</span>
            }
        </Entry>
    );
};

export default TimetablePreviewRegisteredEntry;