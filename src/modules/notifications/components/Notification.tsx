import React from "react";
import { FC } from "react";
import { LinkIcon } from "@chakra-ui/icons";
import styled from "styled-components";

type NotificationProps = {
    text: string;
    link: string | null;
}

const Wrapper = styled.div`
    background: #ffffff;
    border-radius: 0.5rem;
    padding: 1rem;
    margin-bottom: 1rem;
    font-size: 1.25rem;
    font-weight: bold;
    min-width: 60vw;
`;

const Link = styled.a`
    color: #0099ff;
    font-size: 1.5rem;
    text-decoration: none;

    &:hover {
        color: #0000ff;
        text-decoration: underline;
    }
`;

export const Notification: FC<NotificationProps> = ({ text, link }: NotificationProps) => {
    return (
        <Wrapper>
            {text.split("\\n").map((sentence) => <>{sentence}<br/></>)}
            {link && (
                <>
                    <br />
                    <Link href={link} target="_blank">
                        <LinkIcon mr="2"/>
                        {link}
                    </Link>
                </>
            )}
        </Wrapper>
    );
};